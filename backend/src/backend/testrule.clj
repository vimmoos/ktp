(ns backend.testrule
  (:require
   [clara.rules :refer :all]
   [backend.state :as s]))

(def translator {"no" :no
                 "noes" :noes
                 "yeno" :yeno
                 "yes" :yes})

(defn firer [fact ch]
  (-> ch
     s/get-session
     (insert fact)
     fire-rules
     (s/update-session ch)))

(defrecord Pippo [pip])
(defrecord Baudo [baud promosso])
(defrecord Dio [porco])
(defrecord Question [quest])
(defrecord Whoami [ws])

(defrule can
  [Pippo (= pip :yes)]
  [Dio (= porco :yes)]
  =>
  (println "c'e' pippo e pure il dio porco"))

(defrule ti
  [Pippo (= pip :no)]
  [Baudo (= ?baud baud) (= ?promosso promosso)]
  =>
  (println "non c'e' pippo ma c'e' baudo" ?baud ?promosso))


(defrule yespippo
  {:salience 0}
  [Pippo (= pip :yes)]
  =>
  (println "yes pippo"))

(defrule nopippo
  {:salience 0}
  [Pippo (= pip :no)]
  =>
  (println "no pippo"))

(defrule question?
  {:salience 10}
  [Question (= ?quest quest)]
  [Whoami (= ?ws ws)]
  =>
  (s/add-q ?ws ?quest))

(defrule pippo?
  {:salience -8}
  [:not [Pippo]]
  =>
  (insert!
   (->Question
    {:question "C'e' Pippo?"
     :type :excl
     :answers ["yes" "no"]
     :fact ->Pippo})))

(defrule baudo?
  {:salience -8}
  [:not [Baudo nil]]
  =>
  (insert!
   (->Question
    {:answers [["yes" "yeno"] ["no" "noes"]]
     :type :inclg
     :question "C'e' Baudo?"
     :fact ->Baudo})))

(defrule dio?
  {:salience -8}
  [:not [Dio]]
  =>
  (insert!
   (->Question {:answers ["yes" "no"]
                :type :excl
                :question "C'e' DIO?"
                :fact ->Dio})))
