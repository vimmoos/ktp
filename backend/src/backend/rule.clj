(ns backend.rule
  (:require
   [clara.rules :refer :all]
   [backend.state :as s]
   [backend.macros :refer [knowledge-base]]))


(defn firer [fact ch]
  (-> ch
     s/get-session
     (insert fact)
     fire-rules
     (s/update-session ch)))

(knowledge-base
 {:translator {"Yes" :yes
               "No" :no
               "Never" :never
               "Sometimes" :st
               "None" :none
               "Regularly (Daily/Weekly)" :reg

               ["Employee with permanent contract"
                "Artisan/Merchant/Entrepreneur"
                "Self-Employed/Freelancer"
                "Executive manager"] :Pro

               ["Temporary worker" "Unemployed"
                "Retired" "Student"] :Retail

               ["20.000 - 50.000 €" "50.000 - 100.000" "< 15%"
                "Home owned" "50.000 - 80.000" "Future purchases"
                "Future purchases" "Regular income" "Regular income"
                "Capital income"
                "Business income" "Wait for investment to recover"
                "Hoping for a higher than average return I accept taking higher risks and I am ready to accept the slight decrease of my capital." "2-5 years"] :avg

               ["> 80.000" "Hoping for high returns I am ready to take high risks even if a considerable part of my capital is at risk as a result, what is more, if I invest in a derivative product, the loss may exceed even the amount invested in the product concerned." "Wait for value increase"
                "Increase investment to increase chances" "Speculative trading "
                "Value increase" "> 50.000 €" "> 100.000" "> 15%" "Fixed salary"
                "Home owned + other properties" "Speculative trading"
                "Value increase" "> 5 years"] :high

               ["No possession" "Disinvest immediately to avoid further losses"
                "The safety of my capital is the most important to me, therefore, to prevent risks I am ready to give up reaching high returns." "< 20.000 €" "< 50.000"
                "Provision for old age" "Family's safety"
                "0%" "Provision for old age" "Family's safety"
                "Variable salary" "< 2 years"] :low}
  :facts
  {:Occupation
   {:question "What is your occupation?"
    :answers [:excl "Self-Employed/Freelancer" "Employee with permanent contract"
              "Temporary worker" "Retired" "Unemployed" "Executive manager"
              "Student" "Artisan/Merchant/Entrepreneur"]
    :salience 4
    :fields [:job]}

   :Inv-exp
   {:question "Do you have any investement experience?"
    :answers [:excl "Yes" "No"]
    :salience 3
    :fields [:exp]}

   :Instr-known
   {:question "Which of these financial instruments do you know?"
    :answers [:inclg ["Structured product" "Exchange Trade Funds"
                      "Foreign exchange options"]
              ["Investement Funds" "Shares"
               "Governament securities" "Real estate funds"]]
    :salience 4
    :fields [:cpx :spx]}


   :Update-freq
   {:question "How often do you update yourself on finacial market trends?"
    :answers [:excl "Never" "Sometimes" "Regularly (Daily/Weekly)"]
    :salience 4
    :fields [:freq]}



   :Instr-used
   {:question "Which of these financial instruments have you used?"
    :answers [:inclg ["Structured product" "Exchange Trade Funds"
                      "Foreign exchange options"]
              ["Investement Funds" "Shares" "Governament securities"
               "Real estate funds"]]
    :precond {:Inv-exp (= :yes exp)
              :Instr-known (not= 0 (+ (count cpx) (count spx)))}
    :salience 3
    :fields [:cpx :spx]}

   :Inv-freq
   {:question "How often do you give investement instruction?"
    :answers [:excl "Sometimes" "Regularly (Daily/Weekly)"]
    :precond {:Instr-used (not= 0 (+ (count cpx) (count spx)))}
    :salience 3
    :fields [:freq]}

   :Incm-src
   {:question "What is your source of income?"
    :answers [:excl "Fixed salary" "Variable salary"
              "Capital income" "Business income" "None"]
    :salience 2
    :fields [:src]}

   :Net-incm
   {:question "What is your net income?"
    :answers [:excl "< 20.000 €" "20.000 - 50.000 €"
              "> 50.000 €"]
    :precond {:Incm-src (not (= :none src))}
    :salience 2
    :fields [:net]}

   :Wealth
   {:question "What is your wealth (including financial
   assets, real estates and movable)"
    :answers [:excl "< 50.000"
              "50.000 - 100.000" "> 100.000"]
    :salience 2
    :fields [:wealth]}

   :Saving-ability
   {:question "How much of your annual net income can you save?"
    :answers [:excl "0%" "< 15%" "> 15%"]
    :salience 2
    :fields [:sav-ab]}

   :Inv-savings
   {:question "How much of your savings have you already invested?"
    :answers [:excl "0%" "< 15%" "> 15%"]
    :salience 2
    :fields [:inv-sav]}

   :Real-estate
   {:question "Which is your real estates properties?"
    :answers [:excl "No possession" "Home owned"
              "Home owned + other properties"]
    :salience 2
    :fields [:real-estate]}

   :Have-loans
   {:question "Do you have loans?"
    :answers [:excl "Yes" "No"]
    :salience 2
    :fields [:loans]}

   :Long-term-loans
   {:question "What are your long-term loans?"
    :answers [:excl "< 50.000"
              "50.000 - 80.000" "> 80.000"]
    :precond {:Have-loans (= :yes loans)}
    :salience 2
    :fields [:ltl]}

   :Goal-inv
   {:question "What is the typical goal of your investments?"
    :answers [:excl "Provision for old age" "Family's safety"
              "Future purchases" "Regular income"
              "Speculative trading " "Value increase"]
    :salience 1
    :fields [:goal]}

   :Inv-period
   {:question "Period characterizing your investment transactions?"
    :answers [:excl "< 2 years"
              "2-5 years" "> 5 years"]
    :salience 1
    :fields [:period]}

   :Reaction
   {:question "What is your reactions to negative markets moviments?"
    :answers [:excl "Disinvest immediately to avoid further losses"
              "Wait for investment to recover"
              "Wait for value increase"
              "Increase investment to increase chances"]
    :salience 1
    :fields [:react]}

   :Priority
   {:question "Which one is the most important to you from
   the following statements?"
    :answers [:excl "The safety of my capital is the most important to me, therefore, to prevent risks I am ready to give up reaching high returns."
              "Hoping for a higher than average return I accept taking higher risks and I am ready to accept the slight decrease of my capital."
              "Hoping for high returns I am ready to take high risks even if a considerable part of my capital is at risk as a result, what is more, if I invest in a derivative product, the loss may exceed even the amount invested in the product concerned."]
    :salience 1
    :fields [:prio]}

   :Financial-edu [:edu]
   :Cli-knowledge [:lvl]
   :Cli-exp [:lvl]
   :Cli-exp-knowledge [:lvl]
   :Income-lvl [:lvl]
   :Income+wealth  [:lvl]
   :Saving [:lvl]
   :Sustainability [:lvl]
   :Cli-risk-profile [:lvl]
   :Cli-risk-appetite [:lvl]}

  :No-fin-ed
  [{:or {:Update-freq (= :never freq)
         :Instr-known (and (< (count spx) 2)
                           (< (count cpx) 2))}}
   {:insert [:Financial-edu :low]}]

  :Avg-fin-ed
  [[[:or {:Update-freq (= :st freq)
          :Instr-known (and (>= (count spx) 2)
                            (< (count cpx) 2))}]
    [:or {:Update-freq (= :reg freq)
          :Instr-known (not (and (< (count spx) 2)
                                 (< (count cpx) 2)))}]]

   {:insert [:Financial-edu :avg]}]

  :Good-fin-ed
  [{:Update-freq (= :reg freq)
    :Instr-known (and (>= (count cpx) 2)
                      (or (>= (count cpx) 2)
                          (< (count spx) 2)))}
   {:insert [:Financial-edu :high]}]

  :High-cli-knowledge
  [{:Occupation (= :Pro job)
    :Financial-edu (= :high edu)}
   {:insert [:Cli-knowledge :high]}]

  :Avg-cli-knowledge
  [{:or {:Occupation (not (= :Pro job))
         :Financial-edu (not (= :high edu))}
    :Financial-edu (not (= :low edu))}
   {:insert [:Cli-knowledge :avg]}]

  :Low-cli-knowledge
  [{:Financial-edu (= :low edu)}
   {:insert [:Cli-knowledge :low]}]

  :High-cli-exp
  [[[:Inv-freq (= :reg freq)]
    [:Instr-used (and (>= (count cpx) 2)
                      (or (>= (count cpx) 2)
                          (< (count spx) 1)))]]
   {:insert [:Cli-exp :high]}]

  :Avg-cli-exp
  [[[:or {:Inv-freq (= :st freq)
          :Instr-used (and (>= (count spx) 1)
                           (< (count cpx) 2))}]
    [:or {:Inv-freq (= :reg freq)
          :Instr-used (not (and (< (count spx) 1)
                                (< (count cpx) 2)))}]]
   {:insert [:Cli-exp :avg]}]

  :Low-cli-exp
  [{:or {:Inv-exp (= :no exp)
         :Instr-known (= 0 (+ (count cpx) (count spx)))
         :Instr-used (and (< (count spx) 1)
                          (< (count cpx) 2))}}
   {:insert [:Cli-exp :low]}]

  :Low-cli-ke
  [{:or {:Cli-knowledge (= :low lvl)
         :Cli-exp (= :low lvl)}}
   {:insert [:Cli-exp-knowledge :low]}]

  :Avg-cli-ke
  [[[:or {:Cli-knowledge (not (= :low lvl))
          :Cli-exp (= :high lvl)}]
    [:or {:Cli-knowledge  (= :high lvl)
          :Cli-exp (not (= :low lvl))}]
    [:or {:Cli-knowledge  (= :avg lvl)
          :Cli-exp  (= :avg lvl)}]]
   {:insert [:Cli-exp-knowledge :avg]}]

  :High-cli-ke
  [{:Cli-exp (= :high lvl)
    :Cli-knowledge (= :high lvl)}
   {:insert [:Cli-exp-knowledge :high]} ]

  :High-income
  [{:Incm-src (not (or (= :low src)
                       (= :none src)))
    :or {:Incm-src (= :high src)
         :Net-incm (= :high net)}
    :Net-incm (not (= :low net))}
   {:insert [:Income-lvl :high]}]

  :Medium-Income
  [{:Incm-src (= :avg src)
    :Net-incm (= :avg net)}
   {:insert [:Income-lvl :avg]}]

  :Low-Income
  [{:or {:Incm-src (or (= :low src)
                       (= :none src))
         :Net-incm (= :low net)}}
   {:insert [:Income-lvl :low]}]

  :Low-income+wealth
  [{:Income-lvl (not (= :high lvl))
    :or {:Wealth (= :low wealth)
         :Income-lvl (= :low lvl)}
    :Wealth (not (= :high wealth))}
   {:insert [:Income+wealth :low]}]

  :Medium-income+wealth
  [[[:or {:Income-lvl (not (= :high lvl))
          :Wealth (= :low wealth)}]
    [:or {:Income-lvl (not (= :low lvl))
          :Wealth (= :high wealth)}]
    [:or {:Wealth (not (= :low wealth))
          :Income-lvl (= :high lvl)}]]
   {:insert [:Income+wealth :avg]}]

  :High-income+wealth
  [{:Income-lvl (= :high lvl)
    :Wealth (not (= :low wealth))}
   {:insert [:Income+wealth :high]}]

  :Yes-sustainability
  [{:Income+wealth (not (= :low lvl))
    :or {:Have-loans (= :no loans)
         :Long-term-loans (= :low ltl)}}
   {:insert [:Sustainability :yes]}]

  :No-sustainability
  [{:or {:Income+wealth (= :low lvl)
         :Long-term-loans (not (= :low ltl))}}
   {:insert [:Sustainability :no]}]

  :Low-Savings
  [{:or [[:and {:Saving-ability (= :high sav-ab)
                :Inv-savings (or (= :low inv-sav) (= :avg inv-sav))}]
         [:and {:Saving-ability (= :avg sav-ab)
                :Inv-savings (= :low inv-sav)}]]}
   {:insert [:Saving :low]}]

  :Avg-Savings
  [{:Inv-savings (= ?val inv-sav)
    :Saving-ability (= ?val sav-ab)}
   {:insert [:Saving :avg]}]

  :High-Savings
  [{:or [[:and {:Saving-ability (= :avg sav-ab)
                :Inv-savings (= :high inv-sav)}]
         [:and {:Saving-ability (= :low sav-ab)
                :Inv-savings (or (= :high inv-sav) (= :avg inv-sav))}]]}
   {:insert [:Saving :high]}]

  :Low-Cli-risk-profile
  [{:Sustainability (= :yes lvl)
    :Real-estate (not (= :low real-estate))
    :Saving (not(= :high lvl))}
   {:insert [:Cli-risk-profile :low]}]

  :Avg-Cli-risk-profile
  [{:Sustainability (= :yes lvl)
    :or [[:and {:Real-estate (= :low real-estate)
                :Saving nil}]
         [:and {:Real-estate (not= :low real-estate)
                :Saving (= :high lvl)}]]}
   {:insert [:Cli-risk-profile :avg]}]

  :High-Cli-risk-profile
  [{:Sustainability (= :no lvl)}
   {:insert [:Cli-risk-profile :high]}]


  :High-appetite
  [{:Goal-inv (= :high goal)
    :Inv-period (or (= :avg period) (= :high period))
    :Reaction (= :high react) ;; first reaction was SHOCK
    :Priority (= :high prio)}
   {:insert [:Cli-risk-appetite :high]}]

  :Medium-appetite
  [{:Goal-inv (not (= :low goal))
    :Reaction (not (= :low react))
    :Priority (not (= :low prio))
    :not {:and {:Goal-inv (= :high goal)
                :Reaction (= :high react)
                :Priority (= :high prio)}}}
   {:insert [:Cli-risk-appetite :avg]}]

  :Low-appetite
  [{:or {:Goal-inv (= :low goal)
         :Reaction (= :low react)
         :Priority (= :low prio)}}
   {:insert [:Cli-risk-appetite :low]}]

  :Conclusion-1
  [{:Cli-exp-knowledge (= :high lvl)
    :Cli-risk-profile (or (= :high lvl) (= :avg lvl))
    :Cli-risk-appetite nil}
   {:conclusion {:type :end
                 :payload {:title "PORTFOLIO 7/6/5"
                           :link "https://www.moneyfarm.com/uk/portfolios/"
                           :num 6}
                 :conclusion "Final decision: we highly advice you to
                 follow these portfolio for your future
                 investement"}}]
  :Conclusion-2
  [{:Cli-exp-knowledge (= :high lvl)
    :Cli-risk-profile (or (= :high lvl) (= :avg lvl))
    :Cli-risk-appetite nil}
   {:conclusion {:type :end
                 :payload {:title "PORTFOLIO 6/5/4"
                           :link "https://www.moneyfarm.com/uk/portfolios/"
                           :num 5}
                 :conclusion "Final decision: we highly advice you to
                 follow these portfolio for your future
                 investement"}}]

  :Conclusion-3
  [{:Cli-exp-knowledge (= :avg lvl)
    :Cli-risk-profile (or (= :high lvl) (= :avg lvl))
    :Cli-risk-appetite nil}
   {:conclusion {:type :end
                 :payload {:title "PORTFOLIO 5/4/3"
                           :link "https://www.moneyfarm.com/uk/portfolios/"
                           :num 4}
                 :conclusion "Final decision: we highly advice you to
                 follow these portfolio for your future
                 investement"}}]

  :Conclusion-4
  [{:Cli-exp-knowledge (= :avg lvl)
    :Cli-risk-profile (= :low lvl)
    :Cli-risk-appetite nil}
   {:conclusion {:type :end
                 :payload {:title "PORTFOLIO 4/3/2"
                           :link "https://www.moneyfarm.com/uk/portfolios/"
                           :num 3}
                 :conclusion "Final decision: we highly advice you to
                 follow these portfolio for your future
                 investement"}}]

  :Conclusion-5
  [{:Cli-exp-knowledge (= :low lvl)
    :Cli-risk-profile (or (= :high lvl) (= :avg lvl))
    :Cli-risk-appetite nil}
   {:conclusion {:type :end
                 :payload {:title "PORTFOLIO 1/2/3"
                           :link "https://www.moneyfarm.com/uk/portfolios/"
                           :num 2}
                 :conclusion "Final decision: we highly advice you to
                 follow these portfolio for your future
                 investement"}}]

  :Conclusion-6
  [{:Cli-exp-knowledge (= :low lvl)
    :Cli-risk-profile (= :low lvl)
    :Cli-risk-appetite nil}
   {:conclusion {:type :end
                 :payload {:title "PORTFOLIO 1/2"
                           :link "https://www.moneyfarm.com/uk/portfolios/"
                           :num 1}
                 :conclusion "Final decision: we highly advice you to
                 follow these portfolio for your future
                 investement" }}]})
