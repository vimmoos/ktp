(ns backend.macros
  (:require
   [clara.rules :refer :all]
   [backend.state :as s]))

;;;;; WANTED SYNTAX
(def wanted-syntax
  {:translator {"yes" :yes
                "no" :no}
   :facts
   ;; necessary question
   {:name1 {:question "is name a pippo?"
            :answers [:incl "yes" "no"]
            :salience 1
            :fields []}
    ;; secondary question
    :name {:question "is name a pippo?"
           :answers [:incl "yes" "no"]
           :salience 1
           :precond {:name1 ['(bool-sexp) '(bool-sexp)]
                     :not [name]}
           :fields []}
    :name2 [:field1 :field2]}

   :name-rule
   [{:name 'cond
     :name1 'cond1}
    {:insert 'args
     :ask 'args1}]})

(defmulti act->fun first)

(defmethod act->fun :ask
  [[_ body]]
  (act->fun [:insert
             [:Question body]]))

(defmethod act->fun :conclusion
  [[_ body]]
  (act->fun [:insert
             [:Conclusion (assoc-in body [:payload :trace]
                                    '(s/get-trace ?ws))]]))

(defmethod act->fun :insert
  [[_ body]]
  `(clara.rules/insert!
    (~(->> (first body)
           symbol
           (str "->")
           symbol)
     ~@(rest body))))


(defmethod act->fun :print
  [[_ body]]
  `(println ~@(if (seq? body)
                body
                (list body))))

(defmethod act->fun :sexp
  [[_ body]] body)



(defn parse-precond [[name value]]
  (cond
    (some #{name} [:or :and :not]) `[~name ~@(map parse-precond value)]
    (= name :salience) `{:salience ~(* -1 value)}
    :else `[~(symbol name) ~@(cond
                               (vector? value) value
                               (nil? value) []
                               :else [value])]))


(defn parse-rule [[name [precond postcond :as rule] ]]
  `(defrule ~(symbol name)
     ~@(map parse-precond precond)
     [~(symbol :Whoami) (= ~(symbol :?ws) ~(symbol :whoami))]
     =>
     (s/add-action-trace ~(symbol :?ws) '{~name
                                          ~rule})
     ~@(map act->fun postcond)))

(defmulti parse-facts
  (fn [[name val]]
    (if (map? val) :map :vec)))

(defmethod parse-facts :vec
  [[name fields]]
  `(defrecord ~(symbol name)
       [~@(map symbol fields)]))


(defmethod parse-facts :map
  [[name {fields :fields}]]
  (parse-facts [name fields]))

(defn parse-facts-rules
  [[name {question :question
          anss :answers
          sal :salience
          precond :precond}]]
  (let [fact (symbol name)]
    (parse-rule [(symbol (str name "?"))
                 [(merge {:salience sal
                          :not {fact nil}}
                         precond)
                  {:ask
                   {:question question
                    :answers (subvec anss 1)
                    :type (first anss)
                    :fact (-> "->"
                             (str fact)
                             symbol)}}]])))

(defn deftranslator [name translator]
  `(def ~name
     ~(reduce-kv (fn [acc k v]
                   (merge acc
                          (cond
                            (coll? k) (reduce #(assoc %1 %2 v) {} k)
                            :else {k v})))
                 {} translator)))

(defmacro knowledge-base [edn]
  `(do
     ~(deftranslator (symbol :translator) (:translator edn))
     (defrecord ~(symbol :Question) [~(symbol :quest)])
     (defrecord ~(symbol :Whoami) [~(symbol :whoami)])
     (defrecord ~(symbol :Conclusion) [~(symbol :payload)])
     (defrule ~(symbol :question?)
       [~(symbol :Question) (= ~(symbol :?quest) ~(symbol :quest))]
       [~(symbol :Whoami) (= ~(symbol :?ws) ~(symbol :whoami))]
       =>
       (s/add-q ~(symbol :?ws) ~(symbol :?quest)))
     (defrule ~(symbol :conclusion?)
       [~(symbol :Conclusion) (= ~(symbol :?payload) ~(symbol :payload))]
       [~(symbol :Whoami) (= ~(symbol :?ws) ~(symbol :whoami))]
       =>
       (s/add-q ~(symbol :?ws) ~(symbol :?payload)))

     ~@(map parse-facts (:facts edn))

     ~@(map (fn [[x y :as args] ]
              (when (map? y)
                (parse-facts-rules args))) (:facts edn))

     ~@(map parse-rule (dissoc edn :facts :translator))))
