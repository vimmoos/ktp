(ns backend.core
  (:require [org.httpkit.server :as server]
            [clara.rules :as r]
            [backend.state :as state]
            [backend.parser :as parser]
            [mount.core :refer [defstate start stop]]
            [taoensso.timbre :as timbre :refer [log info warn report]]
            [backend.rule :as rule]))

(defn handshake [ws]
  (report "Connected WS:" ws))

(defn cleanup [ws status]
  (state/rm-client ws)
  (report "Disconnected WS:" ws
          " with status:" status ))

(defmulti handler
  (fn [ws msg]
    (println (-> msg
                read-string
                :trace))
    (println  msg)
    (if (=  (-> msg
               read-string
               :type):trace) :trace :fact)))


(defmethod handler :fact
  [ws msg]
  (report "Handle msg from:" ws)
  (-> msg
     read-string
     (assoc :ch ws)
     parser/msg->fact
     (rule/firer ws))
  (let [question (-> ws
                    state/next-q
                    str)]
    (report "Sending question:" question
            " To ws:" ws)
    (server/send! ws question)))

(defmethod handler :trace
  [ws _]
  (report "Handler trace msg from:" ws)
  (server/send! ws (str (parser/prod-trace ws))))

(defstate server
  :start (server/run-server
          (fn [req]
            (server/as-channel
             req
             {:on-open handshake
              :on-close cleanup
              :on-receive handler}))
          {:port 8091
           :host "0.0.0.0"})
  :stop (server))

(defn startup []
  (start #'state/state #'server))
