(ns backend.state
  (:require
   [mount.core :refer :all]))

(defstate state
  :start (atom {})
  :stop nil)


(defn add-client [ws session]
  (swap! state assoc ws {:session session
                         :trace []
                         :questions '()
                         :quest nil}))

(defn rm-client [k]
  (swap! state dissoc k))

(defn get? [k ws]
  (get-in @state [ws k]))

(defn add-? [key ws ?]
  (swap! state assoc-in [ws key]
         (-> key
            (get? ws)
            (conj ?))))


(def add-q (partial add-? :questions ))

(def add-action-trace (partial add-? :trace))

(defn get-trace [ws]
  (get-in @state [ws :trace]))



(defn add-c [ws c]
  (swap! state assoc-in [ws :questions]
         (list c)))

(defn next-q [ws]
  (let [next (-> :questions
                 (get? ws)
                 peek)]
    (swap! state assoc-in [ws :questions] (-> :questions
                                              (get? ws)
                                              pop))
    (swap! state assoc-in [ws :quest] next)
    (dissoc next :fact)))

(defn get-session [ws]
  (get? :session ws))

(defn update-session [new-sess ws]
  (swap! state assoc-in [ws :session] new-sess))
