(ns backend.parser
  (:require [backend.state :as state]
            [taoensso.timbre :as timbre :refer [log info warn report]]
            [clara.rules :as r]
            [backend.rule :as rule]))


(defmulti msg->fact
  (fn [{type :type :as msg}]
    (report "Parsing msg:" msg)
    (report "Translated payload"
            (cond
              (= type :excl) [(-> (:payload msg)
                                 rule/translator)]
              (= type :incl) [(-> rule/translator
                                 (map  (:payload msg))
                                 vec)]
              :else  (-> #(vec (map rule/translator %))
                        (map  (:payload msg))
                        vec)))
    type))


(defmethod msg->fact :excl
  [{ch :ch payload :payload}]
  (-> @state/state
     (get-in [ch :quest :fact])
     (apply [(rule/translator payload)])))

(defmethod msg->fact :incl
  [{ch :ch payload :payload}]
  (-> @state/state
     (get-in [ch :quest :fact])
     (apply [(-> rule/translator
                (map  payload)
                vec)])))

(defmethod msg->fact :inclg
  [{ch :ch payload :payload}]
  (let [payload (if (empty? payload)
                  [[] []]
                  payload)]
    (-> @state/state
       (get-in [ch :quest :fact])
       (apply (-> #(vec (map rule/translator %))
                 (map  payload)
                 vec)))))



(defmethod msg->fact :start
  [{ch :ch}]
  (state/add-client
   ch
   (r/mk-session 'backend.rule))
  (-> ch
     rule/->Whoami))

(defn prod-trace [ws ]
  {:type :trace
   :trace (state/get-trace ws)})
