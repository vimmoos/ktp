% Created 2021-01-14 gio 07:12
% Intended LaTeX compiler: pdflatex
\documentclass[11pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage{minted}
\usepackage{apacite}
\author{Marco gallo}
\date{\today}
\title{}
\hypersetup{
 pdfauthor={Marco gallo},
 pdftitle={},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 27.1 (Org mode 9.4.4)},
 pdflang={English}}
\begin{document}

\input{./rug_title_page.tex}

\newpage
  \pagenumbering{roman}
  \tableofcontents
\newpage
\pagenumbering{arabic}

\section{Rule model}
\label{sec:org29972f3}
\texttt{clara-rules}, a forward-chaining inference engine written in Clojure(Script),
was employed as the main reasoning backend. As nearly all modern
forward-chaining rules engine, Clara is a variation of the Rete algorithm
\cite{forgy1989rete}. The typical layout of a Rete network is depicted
in \hyperref[p:rete-topology]{Figure 1}.

\begin{figure}[!htpb]
\centering
\includegraphics[width=.9\linewidth]{./1200px-Rete.svg.png}
\caption{\label{p:rete-topology}The topology of a Rete network}
\end{figure}

At a high-level, the Rete algorithm organizes rules and facts into a tree. The
root of the tree is the so-called \emph{working memory}, a collection of facts that
are true at some point in time of the inference process (in Clara this is
called \texttt{session}). Each node in the tree (except the root) represents a
left-hand-side (LHS) condition of a rule occurring in the knowledge base. The
path from the root node to a leaf node corresponds to the complete LHS of a rule.
Leaf nodes contain the right-hand-side (RHS) of a rule; when a fact that matches
a rule's LHS completely is present in the working memory, the rule's RHS (the
leaf node) fires, and the effects defined in said RHS activate. For this reason,
leaf nodes are also called \emph{production nodes}. The process described so far
is repeated over and over, until no rule can fire anymore - as dictated by
forward-chaining. Lastly, the Rete algorithm is usually paired with a
mechanism of \emph{conflict resolution}. Once all rules that have matching
preconditions in the working memory are determined by a pass of the algorithm,
all the leaf nodes (productions) which can be triggered are put in the so-called
\emph{conflict set}. The conflict resolution mechanism then determines the order of
firing of the conflicting productions according their \emph{salience}, or rule
priority.

The Rete algorithm is aimed to be suitable for highly complex, rule-based logical
models. A key time optimization occurs in the design of nodes: each node has its
own working memory, where facts that match the node's pattern
are saved. When new facts are inferred or modified, they propagate along the
network/tree and the nodes' working memories are updated accordingly: if a node's
pattern (a rule's LHS) matches the fact, the note's working memory is annotated
with that fact. This design sacrifices memory for efficiency, as matches are
computed incrementally and do not need be recomputed for all facts in the session
at each iteration of the inference process.

More specifically, nodes are organized as follows. The direct children of the
root node are \emph{fact type} nodes, which check whether a fact's type matches the
fact type of rule conditions. The next nodes of the network are organized into
\emph{alpha} and \emph{beta} nodes.

\subsection{Alpha-nodes}
\label{sec:org3823413}
Alpha-nodes are also called one-input nodes. In fact, their job is to test
constraints in rule conditions that only involve a single fact at a time. When
a fact matches an alpha-node's pattern, it is propagated to the latter's
children. These can either be a production node (a leaf) if the matched rule only
had a single constraint, or beta-nodes. As described above, an alpha-node has a
memory (\emph{alpha memory}) that holds an history of facts that matched with the
node's pattern. An example of rule matched by an alpha-node can be seen in
\hyperref[c:alpha-node]{Listing 1}, with the corresponding Rete network in \hyperref[p:alpha-node]{Figure 2}.

\begin{listing}[htbp]
\begin{minted}[,frame=single]{clojure}
(defrule Low-cli-knowledge
[Financial-edu (= :no-ed edu)]
=>
(insert! (->Cli-knowledge :low)))
\end{minted}
\caption{\label{c:alpha-node}Example of rule matched by an alpha-node}
\end{listing}

\begin{figure}[!htpb]
\centering
\includegraphics[width=.9\linewidth]{./alpha-node.png}
\caption{\label{p:alpha-node}Rete network of the alpha-node for \hyperref[c:alpha-node]{Listing 1}}
\end{figure}

\subsection{Beta-nodes}
\label{sec:org3be015d}
Beta-nodes are also called two-input nodes. This is explained by the role of
beta-nodes in the network: they are responsible for matching rule constraints
which involve two or multiple facts. As such, a beta-node receive input from
multiple alpha-nodes, and joins together the facts passed down by their parent
according to some pattern specified in the beta-node itself. Moreover, since they
are also endowed with a memory (\emph{beta memory}), they are able to remember input
that matched from one edge, and to later resume it when a new input that matches
is received from the other edge. An example of rule matched by a beta-node
can be seen in \hyperref[c:beta-node]{Listing 2}, with its corresponding network diagram in \hyperref[p:beta-node]{Figure 3}.

\begin{listing}[htbp]
\begin{minted}[,frame=single]{clojure}
(defrule Low-cli-exp-knowledge
[:or [Cli-knowledge (= ?level lvl)]
     [Cli-exp (= knowledge-exp-low? ?level lvl)]]
=>
(insert! (->Cli-exp-knowledge :low)))
\end{minted}
\caption{\label{c:beta-node}Example of rule matched by a beta-node}
\end{listing}

\begin{figure}[!htpb]
\centering
\includegraphics[width=.9\linewidth]{./beta-node.png}
\caption{\label{p:beta-node}Rete network of the beta-node for \hyperref[c:beta-node]{Listing 2}}
\end{figure}



\bibliographystyle{apacite}
\bibliography{bib}
\end{document}