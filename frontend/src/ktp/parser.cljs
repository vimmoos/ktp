(ns ^:figwheel-hooks  ktp.parser
  (:require
   [ktp.init :as init]
   [ktp.conclusion :as c]
   [ktp.trace :as trace]
   [ktp.question :as q]))


(defmulti msg->comp
  (fn [{type :type :as msg}]
    (println msg)
    type))

(defmethod msg->comp :incl
  [{question :question answers :answers}]
  [q/inclusive-question question answers])

(defmethod msg->comp :inclg
  [{question :question answers :answers}]
  [q/inclusive-question-group question answers])

(defmethod msg->comp :excl
  [{question :question answers :answers}]
  [q/exclusive-question question answers])

(defmethod msg->comp :end
  [{conclusion :conclusion payload :payload}]
  [c/conclusion conclusion payload])

(defmethod msg->comp :start
  [_]
  [init/init-button])

(defmethod msg->comp :trace
  [{trace :trace}]
  [trace/trace-comp trace])
