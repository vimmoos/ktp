(ns ^:figwheel-hooks ktp.about
  (:require
   [reagent.core :as re]
   [re-com.core :as r]
   [ktp.parser :as parser]
   [ktp.state :as state]))


(defn about-page []
  [r/v-box
   :size "auto"
   :class "about"
   :gap "1rem"
   :children
   [[r/box :child ""
     :class "banner"
     :size "auto"
     :style
     {:background-image "url(assets/finanical-mathematics_herobanner.jpg)"
      :background-size "contain"
      :background-position "center"
      :background-attachment "fixed"
      :min-height "30rem" }]
    [r/v-box
     :class "content-cards"
     :justify :center
     :align :start
     :style {:flex-flow "row wrap"
             :min-height "50rem"}
     :size "0 1 auto"
     :children
     [[:h2 {:class "text-primary"}
       "Report of the system"]
      [:embed {:src "assets/KTP_report.pdf" :type "application/pdf"
               :width "100%" :height"700px" }]
      ;; <embed src="files/Brochure.pdf" type= width="100%" height="600px" />
      ]]]])
