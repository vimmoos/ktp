(ns ^:figwheel-hooks ktp.tracepage
  (:require
   [ktp.state :as state]
   [re-com.core :as re]
   [ktp.parser :as parser]))

(defn trace-page []
  [re/h-box
   :class "content-cards"
   :justify :center
   :align :start
   :style {
           :flex-flow "row wrap"
           :min-height "50rem"}
   :size "0 1 auto"
   :children
   [[:h2 {:class "text-primary"} "Fired Rules"]
    (parser/msg->comp
     @state/trace-state)]])
