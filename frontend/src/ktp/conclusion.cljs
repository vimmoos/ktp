(ns ^:figwheel-hooks  ktp.conclusion
  (:require
   [reagent.core :as re]
   [re-com.core :as r]
   [ktp.state :as s]
   [ktp.card :refer [card v-card h-card]]))

(defn conclusion
  [conclusion-str {title :title
                   link :link
                   num :num}]
  [v-card
   :style-template :fancy
   :justify :start
   :size "0 1 auto"
   :children
   [[:h2 {:class "text-primary"} title]
    [r/p {:style {:text-align "center"}} conclusion-str] ;; TODO
    [:img {:src (str "assets/imgs/p" num ".png")}]
    ;; fancier maybe?
    [r/h-box
     :gap "10rem"
     :children
     [
      [r/button
       :label "Back Home"
       :class "btn btn-primary btn-lg"
       :on-click #(s/next-q {:type :start})
       :style {:font-size "1.5rem"
               :height "5rem"
               :width "10rem"}
       :tooltip "Back to home page"]
      [r/button
       :label "More info"
       :class "btn btn-primary btn-lg"
       :on-click #(js/window.open link link)
       :style {:font-size "1.5rem"
               :height "5rem"
               :width "10rem"}
       :tooltip "info portfolio"]]]]])
