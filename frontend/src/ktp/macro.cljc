(ns ^:figwheel-hooks ktp.macro
  (:require
   [reagent.core :as reagent]))

(defn make-cursor [state name]
  `(def ~(-> name
            (str "-state")
            symbol)
     (reagent.core/cursor @~state [~(keyword name)])))

(defmacro make-cursors [state & symbols]
  `(do
     ~@(map (partial make-cursor state) symbols)))
