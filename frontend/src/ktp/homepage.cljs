(ns ^:figwheel-hooks ktp.homepage
  (:require
   [reagent.core :as re]
   [re-com.core :as r]
   [ktp.parser :as parser]
   [ktp.state :as state]))


(defn main-page []
  [r/v-box
   :size "auto"
   :class "homepage"
   :gap "1rem"
   :children
   [[r/box :child ""
     :class "banner"
     :size "auto"
     :style
     {:background-image "url(assets/finanical-mathematics_herobanner.jpg)"
             :background-size "contain"
             :background-position "center"
             :background-attachment "fixed"
             :min-height "30rem" }]
    [r/h-box
     :class "content-cards"
     :justify :center
     :align :start
     :style {:flex-flow "row wrap"
             :min-height "50rem"}
     :size "1 0 auto"
     :children [(parser/msg->comp
                 @state/question-state)]]]])
