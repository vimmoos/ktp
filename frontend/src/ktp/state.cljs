(ns ^:figwheel-hooks ktp.state
  (:require-macros [mount.core :refer [defstate]]
                   [ktp.macro :refer [make-cursors]]
                   [cljs.core.async.macros :refer [go]])
  (:require
   [reagent.core :as reagent]
   [cljs.core.async :refer [<! chan >!] :as as]
   [mount.core :as m]))

(m/in-cljc-mode)



(defstate global-state
  :start (reagent/atom {:page {:cur-page nil
                               :pre-page nil}
                        :ws nil
                        :trace {:type :trace
                                :trace nil}
                        :question {:type :start}})
  :stop (reset! @global-state {}))

(make-cursors global-state
              page
              question
              trace)

(defn set-ws [ws]
  (swap! @global-state assoc :ws ws))


(defn get-ws [direction]
  (-> :ws
   (@@global-state)
   direction))

(def get-ws-in (partial get-ws :in))

(def get-ws-out (partial get-ws :out))

(defn switch?
  ([atom new] (reset! atom  new))
  ([atom key-current key-previous new]
   (swap! atom assoc key-previous
          (key-current @atom))
   (swap! atom assoc key-current new)))

(defn previous?
  [atom key-current key-previous new]
  (let [current (key-current @atom)]
    (swap! atom assoc key-current
           (key-previous @atom))
    (swap! atom assoc key-previous new)))

(def switch-page
  (partial switch? page-state :cur-page :pre-page))

(def previous-page
  (partial previous? page-state :cur-page :pre-page))

(def next-q
  (partial switch? question-state))

(def next-trace
  (partial switch? trace-state))

(defn q-go! [what]
  (go
    (-> (get-ws-out)
        (>! what))
    (-> (get-ws-in)
        <!
        next-q)))

(defn trace-go! []
  (go
    (-> (get-ws-out)
       (>! {:type :trace}))
    (-> (get-ws-in)
       <!
       next-trace)))
