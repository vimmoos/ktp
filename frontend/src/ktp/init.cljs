(ns ^:figwheel-hooks ktp.init
  (:require
   [re-com.core :as r]
   [ktp.state :as s]
   [ktp.card :refer [card v-card h-card]])
  (:require-macros [cljs.core.async.macros :refer [go go-loop]]))


(defn init-button []
  [r/button
   :label "START"
   :class "btn btn-primary btn-lg"
   :on-click (fn []
               (s/q-go!  {:type :start})
               (s/trace-go!))
   :attr {:type "button"}
   :style {:padding "1rem"
           :font-size "3rem"
           :border-radius "0.5rem"
           :box-shadow "0 20px 40px -14px
           var(--primary-box-shadow-color-darker)"
           :height "10rem"
           :width "16rem"}
   :tooltip "start?"])
