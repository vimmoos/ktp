(ns ^:figwheel-hooks ktp.app
  (:require
   [ktp.skel :as skel]
   [ktp.header :as header]
   [ktp.state :as state]
   [ktp.init :as init]
   [ktp.homepage :as home]
   [ktp.about :as about]
   [cljs.core.async :refer [<! chan >!] :as as]
   [cljs-websockets-async.core :as websocket]
   [ktp.tracepage :as trace]
   [re-com.core :as re]))


(defn make-tabs [& tabs-map]
  (vec (map
        (fn [{icon :icon on-click :on-click tooltip :tooltip}]
          [re/button
           :class "btn btn-lg"
           :label
           [:i {:class icon
                :style {:font-size "2rem"}}]
           :attr {:on-click on-click
                  :type "button"}
           :style {:cursor "pointer"}
           :tooltip tooltip
           :style {:border-color "none"}]) tabs-map)))


(defn init-app []
  (state/switch-page [home/main-page]))


(defn app []
  (init-app)
  #_(state/switch-page [trace/trace-page])
  (fn []
    [skel/skel
     :header [header/header-skel
              :title "Knowledge Technology Practical"
              :tabs
              (make-tabs {:icon "fas fa-home"
                          :tooltip "Home"
                          :on-click #(init-app)} {:icon "fas fa-atlas"
                          :tooltip "Trace"
                          :on-click (fn []
                                      (state/trace-go!)
                                      (state/switch-page [trace/trace-page]))}
                         {:icon "fas fa-address-card"
                          :tooltip "Info project"
                          :on-click (fn []
                                      (state/switch-page [about/about-page]))})]
     :header-style {:align-items "center"
                    :min-height "7rem"}
     :content (:cur-page @state/page-state)]))
