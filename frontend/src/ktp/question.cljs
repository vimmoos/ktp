(ns ^:figwheel-hooks  ktp.question
  (:require
   [reagent.core :as re]
   [re-com.core :as r]
   [ktp.state :as s]
   [ktp.card :refer [card v-card h-card]]))



(defn abs-question
  [aanswer ans-type question  & possible-answers]
  [v-card
   :style-template :fancy
   :justify :start
   :size "0 1 auto"
   :children
   [[:h2 {:class "text-primary"} question]
    [r/v-box
     :children
     (vec possible-answers)]
    [r/button
     :label "Confirm"
     :class "btn btn-primary btn-lg"
     :on-click #(if @aanswer
                  (s/q-go! {:type ans-type
                            :payload @aanswer})
                  (js/alert "You have to answer!"))
     :style {:font-size "2rem"
             :height "5rem"
             :width "10rem"}
     :tooltip "confirm?"]]])

(defn exclusive-question [question anss]
  (let [atoms (repeatedly (count anss) (partial re/atom nil))
        aanswer (re/atom nil)]
    (apply abs-question
           aanswer
           :excl
           question
           (doall
            (map
             (fn [ans ato]
               [r/checkbox
                :label ans
                :model ato
                :on-change (fn [new-state]
                             (reset! ato new-state)
                             (if new-state
                               (do
                                 (doseq [a (remove #{ato} atoms)]
                                   (reset! a false))
                                 (reset! aanswer ans))
                               (reset! aanswer nil)))])
             anss atoms)))))

(defn inclusive-question [question anss]
  (let [atoms (repeatedly (count anss) (partial re/atom nil))
        aanswer (re/atom [])]

    (apply abs-question
           aanswer
           :incl
           question
           (doall
            (map
             (fn [ans ato]
               [r/checkbox
                :label ans
                :model ato
                :on-change
                (fn [new-state]
                  (reset! ato new-state)
                  (if new-state
                    (swap! aanswer conj ans)
                    (swap! aanswer #(->> %
                                         (remove #{ans})
                                         vec))))]) anss atoms)))))

(defn inclusive-question-group [question ansss]
  (let [atomss (map (fn [anss]
                      (repeatedly
                       (count anss)
                       (partial re/atom nil)))
                    ansss)
        aanswer (re/atom (vec(repeat (count ansss) [])))]
    (apply abs-question
           aanswer
           :inclg
           question
           (doall
            (shuffle (mapcat
                      (fn [anss atoms idx]
                        (map
                         (fn [ans ato]
                           [r/checkbox
                            :label ans
                            :model ato
                            :on-change
                            (fn [new-state]
                              (reset! ato new-state)
                              (if new-state
                                (swap! aanswer update idx
                                       #(-> %
                                           (conj ans)
                                           vec))
                                (swap! aanswer update idx
                                       #(->> %
                                             (remove #{ans})
                                             vec))))])
                         anss atoms))
                      ansss atomss (range 0 (count ansss))))))))
