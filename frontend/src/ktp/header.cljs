(ns ^:figwheel-hooks ktp.header
  (:require
   [re-com.core :as r]))

(defn header-skel [& {:keys [title title-style tabs tabs-style]
                      :or {title "KTP website"
                           title-style {}
                           tabs-style {}}}]
  "Header skeleton, it takes arguments as keywords and it produce an
                      header with a title and a centered tabs
                      section. All the style can be overriden besides
                      for the wrapping div which however has a
                      html-class 'header' so it can be modified in the
                      global css file. The title has html-class
                      'title' and the tabs-box has html-class 'tabs'"
  (fn []
    [r/v-box
     :size "auto"
     :class "header"
     :align :center
     :gap "1rem"
     :children
     [[r/box
       :class "title"
       :child
       [:h1 {:style title-style} title]]
      [r/h-box
       :class "tabs"
       :size "auto"
       :gap "3rem"
       :align :end
       :style (merge   tabs-style)
       :children tabs]]]))
