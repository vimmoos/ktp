(ns ^:figwheel-hooks ktp.core
  (:require
   [goog.dom :as gdom]
   [reagent.core :as reagent :refer [atom]]
   [re-com.core :as r]
   [reagent.dom :as rdom]
   [ktp.app :as a]
   [cljs.core.async :refer [<! chan >!] :as as]
   [cljs-websockets-async.core :as websocket]
   [ktp.state :as state])
  (:require-macros [cljs.core.async.macros :refer [go go-loop]]))

;; DO NOT TOUCH !!!
(def api "139.162.245.31:8091")

(defn init-ws []
  (when-not (state/get-ws-out)
    (go
      (->  (str "ws://" api )
          websocket/connect!
          <!
          state/set-ws))))


(defn mount-app-element []
  (when-let [el (gdom/getElement "app")]
    (init-ws)
    (rdom/render [a/app] el)))

;; conditionally start your application based on the presence of an "app" element
;; this is particularly helpful for testing this ns without launching the app
(mount-app-element)

;; specify reload hook with ^;after-load metadata
(defn ^:after-load on-reload []
  (mount-app-element)
  ;; optionally touch your app-state to force rerendering depending on
  ;; your application
  ;; (swap! app-state update-in [:__figwheel_counter] inc)
  )
