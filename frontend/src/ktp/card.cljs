(ns ^:figwheel-hooks ktp.card
  (:require
   [re-com.core :as r]))


(def card-style-templates
  {:default {
             :min-width "20rem"}
   :fancy {
           :border-radius "0.25rem"
           :box-shadow "0 20px 40px -14px
           var(--primary-box-shadow-color-darker)"
           :padding "1rem"
           :min-width "20rem"}})

(defn card [& {:keys [child style class attr size align justify
                      style-template]
               :or {style {}
                    child ""
                    class ""
                    attr {}
                    align :center
                    justify :center
                    style-template :default
                    size "auto"}}]
  [r/box :child child
   :class (str "card " class)
   :attr attr
   :size size
   :align align
   :justify justify
   :style (merge
           (style-template card-style-templates)
           style)])

(defn ?-card [box-function flex-flow]
  (fn [& {:keys [children style class
                attr size align justify
                gap style-template]
         :or {style {} class "" attr {}
              align :center justify :center
              style-template :default
              gap "1rem"
              size "auto"}}]
    [box-function
     :children children
     :class (str "card " class)
     :attr attr
     :size size
     :gap gap
     :align align
     :justify justify
     :style (merge (style-template card-style-templates)
                   {:flex-flow flex-flow}
                   style)]))

(def v-card (?-card r/v-box "column wrap"))
(def h-card (?-card r/h-box "row wrap"))
