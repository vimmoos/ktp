(ns ^:figwheel-hooks ktp.skel
  (:require
   [re-com.core :as r]))


(defn skel [& {:keys [header content footer header-style
                      content-style footer-style]
               :or {header "Header"
                    header-style {}
                    content "Content"
                    content-style {}
                    footer "Footer"
                    footer-style {}}}]
  "Base skeleton, this create a basic page with an header,content and
                      footer. Each single section style can be
                      modified with the keyword :'section'-style. The
                      html-classes are the same as the name of the
                      section plus the global div which has 'page' as
                      html-class"
  [r/v-box
   :class "page"
   :size "auto"
   :gap "1rem"
   :style {:flex-flow "column wrap"}
   :children
   [[r/box :child header
     :size "auto"
     :class "header"
     :style (merge {:min-height "7rem"} header-style)]
    [r/box :child content
     :class "content"
     :size "auto"
     :style (merge {:min-height "40rem" } content-style) ]
    [r/box :child  footer
     :justify :center
     :size "auto"
     :class "footer"
     :style (merge {:background-color "var(--dark-color-dark)"
                    :align-content "space-evenly"
                    :min-height "7rem" } footer-style)]]])
