(ns ^:figwheel-hooks ktp.trace
  (:require
   [ktp.state :as state]
   [re-com.core :as re]
   [ktp.card :refer [card v-card h-card]]))

;; [{:No-sustainability
;;   [{:or {:Income+wealth (= :low 'lvl),
;;          :not {:Long-term-loans (= :low 'ltl)}}}
;;    {:insert [:Sustainability :no]}]}]

(defn precond-comp [[name value]]
  (cond
    (some #{name} [:or :not :and])
    [v-card
     :style-template :fancy
     :style {:padding "0"
             :margin "0"}
     :justify :start
     :gap "0"
     :size "0 1 auto"
     :children
     [[:h4 {:class "text-secondary"} (str name)]
      [re/v-box
       :style {:padding "0"
               :margin "0"}
       :align :start
       :justify :start
       :children
       (doall
        (vec
         (map precond-comp value)))]]]
    (= name :salience) [:h4 {:class "text-secondary"} (str name " = " value)]
    :else [h-card
           :style-template :fancy

           :style {:padding "0"
                   :margin "1rem"}
           :justify :start
           :size "0 1 auto"
           :children
           [[:h5 {:class "text-secondary"} (str name)]
            [:h5 {:class "text-secondary"} (str  value)]]]))

(defn postcond-comp [[name body]]
  [h-card
   :style-template :fancy
   :style {:padding "0"
           :margin "0"}
   :size "0 1 auto"
   :children
   [[:h4 {:class "text-secondary"} (str name)]
    [re/p {:class "text-secondary"} (str body)]]])

(defn rule-comp [[name [precond postcond]]]
  [v-card
   :style-template :fancy
   :justify :start
   :size "0 1 auto"
   :children
   [[:h2 {:class "text-primary"} name]
    [:h3 {:class "text-primary"} "Pre Conditions"]
    [re/v-box
     :justify :center
     :align :start
     :children
     (doall
      (vec
       (map precond-comp  precond)))]
    [:h3 {:class "text-primary"} "Post Conditions"]
    [re/v-box
     :children
     (doall
      (vec
       (map postcond-comp postcond)))]]])

(defn trace-comp [trace]
  [re/h-box
   :style {:flex-flow "row wrap"}
   :size "0 1 auto"
   :children
   (doall
    (vec
     (reverse
      (distinct
       (map (comp rule-comp
                  (partial reduce
                           (partial into [])))
            trace)))))])
