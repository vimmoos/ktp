#+options: toc:nil
#+latex_header: \mode<beamer>{\usetheme{Madrid}}
#+TITLE: Investment Expert System
#+AUTHOR: M. Falzari, M. Gallo, S. Milanese
#+BEAMER_FRAME_LEVEL: 1
* Problem
# Explain financial market and what investment means
** Investing in the financial market
  - Appealing
  - But hard...
# Explain who an advisor is, what it does, what portfolios are
** Financial Advisor
:PROPERTIES:
:BEAMER_env: block
:BEAMER_act: <2->
:END:
  - Portfolios
  - Moneyfarm

* Expert
# Who is Jona??
** Jona Milanese
   - Master student of Financial Economy, Milano-Bicocca

# Explain interviews and explain validation. Keep up the good work !
** Knowldege acquistion process
:PROPERTIES:
:BEAMER_env: block
:BEAMER_act: <2->
:END:
   - Interviews
   - Validation

* Knowledge model
** Problem solving model
   # Risk profile, risk appetite, client's exp, client's knowldege
   - Facts
     - Base
     - Intermediate
   - Rules

** Rule model
:PROPERTIES:
:BEAMER_env: block
:BEAMER_act: <2->
:END:
   - Forward chaining
   - Rete algorithm (Forgy, 1989)
   - =clara-rules=

* User Interface
**  Web Application
  + [[http://139.162.245.31]]
  + ClojureScript
  + Tracing
* Macro system
# ;; systax
** Syntax
#+begin_src clojure
  {:translator {"yes" :yes "no" :no}
   :facts
   {:name1 {:question "is name?"
            :answers [:incl "yes" "no"]
            :salience 1
	    :precond {:name1 [(bool-sexp) (bool-sexp)]
                     :not [fact]}
            :fields []}}
   :name-rule
   [{:name (bool-sexp)
     :name1 (bool-sexp)}
    {:insert 'args
     :ask 'args1}]}
#+end_src

* Reflection
*** [#B]
- Web application: easily accessible
*** [#B]
:PROPERTIES:
:BEAMER_env: block
:BEAMER_act: <2->
:END:
- KB consistency: non-trivial to prove
***  [#B]
:PROPERTIES:
:BEAMER_env: block
:BEAMER_act: <3->
:END:
- Confidence score in a portfolio
